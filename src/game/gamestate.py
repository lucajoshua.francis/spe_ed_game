import datetime
import json


class GameState:
    width: int
    height: int
    cells: [[int]]
    players: []
    you: int
    running: bool
    deadline: datetime

    def toJson(self):
        return json.dumps(self.__dict__)
