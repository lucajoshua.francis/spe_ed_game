import random
import pygame

from .directions import Direction
from .moves import Move


class Player:
    count = 0

    def __init__(self, position: (int, int), dir: Direction, color: (int, int, int), websocket=None):
        self.num = Player.count
        Player.count += 1
        self.direction = dir
        self.speed = 1
        self.position = position
        self.round = 0
        self.color = color
        self.alive = True
        self.ws = websocket
        self.name = ["Luca", "Lars", "Consti", "Phillip", "Arne", "Vivien", "Peter"][random.randint(0, 6)]

    def turn_left(self):
        self.round += 1
        self.direction = self.direction.toLeftTurn()

    def turn_right(self):
        self.round += 1
        self.direction = self.direction.toRightTurn()

    def speed_up(self):
        self.round += 1
        self.speed += 1
        if self.speed > 10:
            self.alive = False

    def slow_down(self):
        self.round += 1
        self.speed -= 1
        if self.speed < 1:
            self.alive = False

    def change_nothing(self):
        self.round += 1

    async def make_move(self):
        move: Move = await self.ws.recv()
        print(move)
        if move == Move.turn_left:
            self.turn_left()
        elif move == Move.turn_right:
            self.turn_right()
        elif move == Move.speed_up:
            self.speed_up()
        elif move == Move.slow_down:
            self.slow_down()
        elif move == Move.change_nothing:
            self.change_nothing()
        else:
            self.alive = False

    def toJson(self):
        return {
            "x": self.position[0],
            "y": self.position[1],
            "direction": self.direction.value,
            "speed": self.speed,
            "active": self.alive,
            "name": self.name
        }
