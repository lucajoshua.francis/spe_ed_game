import datetime
import json
import os

os.environ['PYGAME_HIDE_SUPPORT_PROMPT'] = "hide"
import pygame

from .directions import Direction
from .moves import Move
from .player import Player
from .tile import Tile


class Game:
    def __init__(self, width: int, height: int, players: [Player], fps: int = 5, gui: bool = True, blocksize: int = 10,
                 output: bool = False, rounds_kill_counts:int = 10):
        self.gui = gui
        self.width = width
        self.height = height
        self.output = output
        self.blocksize = blocksize
        self.players: [Player] = players
        self.fps = fps  # Frames per second.
        self.kills_per_player = []
        self.rounds_kill_count = rounds_kill_counts

        if self.output:
            print("Game created with " + str(len(players)) + " players and size ", width, height)

        if self.gui:
            successes, failures = pygame.init()
            self.screen = pygame.display.set_mode((width * self.blocksize, height * self.blocksize))
            self.clock = pygame.time.Clock()

        self.field = []
        for y in range(height):
            rects = []
            for x in range(width):
                rects.append(
                    Tile(pygame.Rect((x * self.blocksize, y * self.blocksize), (self.blocksize, self.blocksize))))
            self.field.append(rects)

        for player in self.players:
            x, y = player.position
            self.field[y][x].player_num = player.num
            self.field[y][x].round = 0
            self.field[y][x].starting_point = True
            self.kills_per_player.append(0)
            if self.gui:
                image = pygame.Surface((blocksize, blocksize))
                image.fill(player.color)
                r, g, b = player.color

                pygame.draw.rect(image, (0, 0, 0), pygame.Rect(0, 0, blocksize, blocksize), int(blocksize / 4))

                pygame.draw.rect(image, (255 - r, 255 - g, 255 - b),
                                 pygame.Rect(blocksize / 3, blocksize / 3, blocksize / 2.5, blocksize / 2.5))
                self.screen.blit(image, self.field[y][x])
                pygame.display.update()

    def returnJson(self, activeplayer: Player) -> str:
        cells = []
        for yCell in range(self.height):
            line = []
            for xCell in range(self.width):
                line.append(self.field[yCell][xCell].player_num)
            cells.append(line)
        playersdict = {}
        for index, playerInfo in enumerate(self.players, 1):
            playersdict[str(index)] = playerInfo.toJson()
        d = datetime.datetime.utcnow()  # <-- get time in UTC
        d = d + datetime.timedelta(0, 10)
        infos = {
            "width": self.width,
            "height": self.height,
            "cells": cells,
            "players": playersdict,
            "you": activeplayer.num,
            "running": activeplayer.alive,
            "deadline": d.isoformat("T") + "Z"
        }
        infos = json.dumps(infos)
        return infos

    def makeMove(self, num: int, move: Move):
        if move == Move.turn_left:
            self.players[num].turn_left()
        elif move == Move.turn_right:
            self.players[num].turn_right()
        elif move == Move.speed_up:
            self.players[num].speed_up()
        elif move == Move.slow_down:
            self.players[num].slow_down()
        elif move == Move.change_nothing:
            self.players[num].change_nothing()

    def update(self):
        # pygame event check -> game close
        if self.gui:
            self.clock.tick(self.fps)
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    quit()
        # update all player locations if player is alive
        for player in self.players:
            if player.alive:
                jump = False
                x, y = player.position
                # make player.speed steps or jump player.speed-2 steps
                for i in range(player.speed):
                    stepsize = 1
                    if player.round % 6 == 0 and player.speed >= 3:
                        if jump:
                            stepsize = player.speed - 1
                    if player.direction == Direction.up:
                        y -= stepsize
                    elif player.direction == Direction.right:
                        x += stepsize
                    elif player.direction == Direction.down:
                        y += stepsize
                    elif player.direction == Direction.left:
                        x -= stepsize
                    player.position = (x, y)
                    # player outside bounds -> dead
                    if x < 0 or x >= self.width or y < 0 or y >= self.height:
                        player.alive = False
                        break
                    # player on already occupied cell -> dead
                    if not self.field[y][x].player_num == -1:
                        if player.round - self.field[y][x].round < self.rounds_kill_count and not self.field[y][x].player_num == player.num:
                            self.kills_per_player[self.field[y][x].player_num-1] += 1
                        player.alive = False
                        break
                    # update current cell
                    self.field[y][x].player_num = player.num
                    self.field[y][x].round = player.round

                    # draw field to screen
                    if self.gui:
                        tile = self.field[y][x]
                        block_size = self.blocksize

                        # Tile
                        block_surface = pygame.Surface((block_size, block_size))
                        pygame.draw.rect(block_surface, player.color, pygame.Rect(0, 0, block_size, block_size))
                        # Tile Edge
                        pygame.draw.rect(block_surface, (0, 0, 0), pygame.Rect(0, 0, block_size, block_size),
                                         int(block_size / 4))
                        self.screen.blit(block_surface, (tile.rect.x, tile.rect.y))

                        # Line
                        line_width = 2
                        line_surface = pygame.Surface((block_size, line_width))
                        r, g, b = player.color
                        line_surface.fill((255 - r, 255 - g, 255 - b))
                        if player.direction == Direction.down:
                            line_surface = pygame.transform.rotate(line_surface, 90)
                            self.screen.blit(line_surface,
                                             (tile.rect.x + block_size / 2 - line_width / 2,
                                              tile.rect.y - block_size / 2))
                        elif player.direction == Direction.right:
                            self.screen.blit(line_surface,
                                             (tile.rect.x - block_size / 2,
                                              tile.rect.y + block_size / 2 - line_width / 2))
                        elif player.direction == Direction.up:
                            line_surface = pygame.transform.rotate(line_surface, 90)
                            self.screen.blit(line_surface,
                                             (tile.rect.x + block_size / 2 - line_width / 2,
                                              tile.rect.y + block_size / 2))
                        elif player.direction == Direction.left:
                            self.screen.blit(line_surface,
                                             (tile.rect.x + block_size / 2,
                                              tile.rect.y + block_size / 2 - line_width / 2))

                        pygame.display.update()
                    if player.round % 6 == 0 and player.speed >= 3:
                        if jump:
                            break
                        jump = True

    def save_screen(self, path: str = "."):
        pygame.image.save(self.screen, path + "/" + str(datetime.datetime.now().strftime("%Y%m%d-%H%M%S")) + ".jpg")

    def exit(self):
        for player in self.players:
            player.alive = False
        if self.gui:
            pygame.quit()

    def running(self) -> bool:
        run = False
        for player in self.players:
            if player.alive:
                run = True
        return run


if __name__ == '__main__':
    players = [Player((30, 30), Direction.up, (255, 0, 0), 12)]
    game = Game(100, 100, players, 15, True, 12, True)
    while game.running():
        game.update()
    game.save_screen()
    for row in json.loads(game.returnJson(players[0]))["cells"]:
        line = ""
        for cell in row:
            line += str(cell)
        print(line)
    game.exit()
