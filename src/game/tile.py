import pygame


class Tile:
    def __init__(self, rect: pygame.Rect):
        self.rect = rect
        self.player_num = -1
        self.starting_point = False
        self.round = -1
